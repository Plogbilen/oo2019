import java.util.ArrayList;
import java.util.Collections;

public class Deck {
	public ArrayList<Card> cards = new ArrayList<Card>();
	
	public Deck() {
		for(int i = 0; i < 13; i++) {
			for(int j = 0; j < 4; j++) {
				cards.add(new Card(i, j));
			}
		}
		Collections.shuffle(cards);
	}
	
	public void printdeck() {
		for(int i = 0; i < cards.size(); i++) {
			System.out.println(cards.get(i).value);
		}
	}
}
