public class Card {
    public int value;
    public int suit;

    public Card(int value, int suit) {
        this.value = value;
        this.suit = suit;
    }
}